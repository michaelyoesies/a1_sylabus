/// <reference types="cypress" />

describe("Basic Desktop Test", () => {
  //   before(() => {
  //     cy.then(() => {
  //       window.localStorage.setItem("__auth__token", token);
  //     });
  //   });
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");
  });

  it("The webpage load, at least", () => {
    cy.visit("https://codedamn.com");
  });

  it("Login page looks good", () => {
    cy.contains("Login").click();
    cy.contains("Sign in to codedamn").should("exist");
    cy.contains("Forgot your password?").should("exist");
    cy.contains("Remember me").should("exist");
  });

  it("Login page links work", () => {
    // 1. Go to login page
    cy.contains("Login").click();

    // 2. Click the forgot your password
    cy.contains("Forgot your password?").click();

    // 3. Verify the url is right
    cy.url().should("include", "/password-reset");
    cy.url().then((value) => {
      cy.log("The current real URL is: ", value);
    });
    // 4. Go back to sign in page
    cy.go("back");

    // 5. Click the create account
    cy.contains("Create Account").click();

    // 6. Verify the url is right
    cy.url().should("include", "/register");
  });

  it("The webpage load, at least", () => {});

  it("Login should display correct error", () => {
    cy.contains("Login").click();

    cy.contains("Unable to authorize").should("not.exist");

    cy.get("[data-testid=username]").type("admin");
    cy.get("[data-testid=password]").type("admin");

    cy.get("[data-testid=login]").click();

    cy.contains("Unable to authorize").should("exist");
  });

  //   it("Login should work fine", () => {
  //     cy.contains("Login").click();

  //     cy.get("[data-testid=username]").type("iosdev");
  //     cy.get("[data-testid=password]").type("iosdev");

  //     cy.get("[data-testid=login]").click();

  //     cy.url().should("include", "/dashboard");
  //   });

  // Cara menggunakan contains
  // Way 1
  // cy.contains("From zero");
  // Way 2
  // cy.get("div > a");
  // Way 3 better for element change
  // cy.get("[data-testid=logo]").click();

  // Bisa menggunakan viewport pada test berbeda
  //   it("Every basic element exist on mobile", () => {
  //     cy.viewport("iphone-7");
  //     cy.visit("https://codedamn.com");
  //   });
});
