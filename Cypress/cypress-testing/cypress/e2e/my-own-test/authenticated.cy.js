/// <reference types="cypress" />

const token = "smdamdamdwomoiqwenqweuqienqweqweasdn123n21im21i";

describe("Basic Authenticated Desktop Test", () => {
  before(() => {
    cy.then(() => {
      window.localStorage.setItem("__auth__token", token);
    });
  });
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");
  });

  it("Should load playground correctly", () => {
    cy.visit("https://codedamn.com/playground/html");

    cy.log("Checking for sidebar");
    cy.contains("Trying to connect").should("exist");

    cy.log("Checking bottom left button");
    cy.get("[data-testid=xterm-controls] > div").should("contain.text", "Connecting");

    cy.contains("Trying to establish").should("exist");

    cy.log("Playground is initializing");

    cy.contains("Setting up the challenge").should("exist");
    cy.contains("Setting up the challenge", { timeout: 10 * 1000 }).should("not.exist");

    // cy.debug();
  });

  it("New file feature works", () => {
    cy.visit("https://codedamn.com/playground/html");

    cy.contains("Setting up the challenge", { timeout: 10 * 1000 }).should("exist");
    cy.contains("Setting up the challenge", { timeout: 10 * 1000 }).should("not.exist");

    const fileName = Math.random().toString().slice(0, 3);

    cy.get("[data-testid=xterm]").type("{ctrl}{c}").type(`touch testscript.${fileName}.js{enter}`);
    cy.contains(`testscript.${fileName}.js`).should("exist");
  });

  it("New file feature works", () => {
    cy.visit("https://codedamn.com/playground/html");

    cy.contains("Setting up the challenge", { timeout: 10 * 1000 }).should("exist");
    cy.contains("Setting up the challenge", { timeout: 10 * 1000 }).should("not.exist");

    const fileName = Math.random().toString().slice(0, 3);

    cy.get("[data-testid=xterm]").type("{ctrl}{c}").type(`touch testscript.${fileName}.js{enter}`);
    cy.contains(`testscript.${fileName}.js`).should("exist");

    cy.contains(`testscript.${fileName}.js`).rightclick();
    cy.contains("Rename File").click();

    cy.get("[data-testid=renamefilefolder]").type(`new_.${fileName}.js`);

    cy.get("[data-testid=renamebtn]").click();

    cy.contains(`testscript.${fileName}.js`).should("not.exist");
    cy.contains(`new_.${fileName}.js`).should("exist");
  });
});
